// Board.java
package oop.asg04;
/**
CS108 Tetris Board.
Represents a Tetris board -- essentially a 2-d grid
of booleans. Supports tetris pieces and row clearing.
Has an "undo" feature that allows clients to add and remove pieces efficiently.
Does not do any drawing or have any idea of pixels. Instead,
just represents the abstract 2-d board.

*/

import java.util.*;

import java.lang.RuntimeException;

public class Board {
	// Some ivars are stubbed out for you:
	
	private int width;
	private int height;
	private boolean[][] grid;
	private boolean[][] gridB;
	private boolean DEBUG = true;
	boolean committed;
	private int[] heights;
	private int[] widths;
	private int maxHeight;
	
	// Here a few trivial methods are provided:
	
	/**
	 Creates an empty board of the given width and height
	 measured in blocks.
	  
	*/
	public Board(int width, int height) {
	    int i,j;
		this.width = width;
		this.height = height;
		
		widths = new int[height];
		heights = new int[width];
		grid = new boolean[width][height];
		gridB = new boolean[width][height];
		committed = true;
		
		for(i=0;i<width;i++)
		{
		     heights[i]=0;
		}
		
		for(i=0;i<height;i++)
		{
		     widths[i]=0;
		}
		
		for(i=0;i<width;i++)
		{
		     for(j=0;j<height;j++)
			      grid[i][j]=false;
		}
		for(i=0;i<width;i++)
		{
		     for(j=0;j<height;j++)
			      gridB[i][j]=false;
		}
		
		maxHeight=0;
		// YOUR CODE HERE
	}
	
	
	/**
	 Returns the width of the board in blocks.
	 
	*/
	public int getWidth() {
		return width;
	}
	
	
	/**
	 Returns the height of the board in blocks.
	 
	*/
	public int getHeight() {
		return height;
	}
	
	
	/**
	 Returns the max column height present in the board.
	 For an empty board this is 0.
	 
	*/
	public int getMaxHeight() {	 
	    int i=0;
		int maxHeight=heights[0];
		for(i=0;i<width;i++)
		{
		    if(heights[i]>maxHeight)
			    maxHeight=heights[i];
		}
		return maxHeight; // YOUR CODE HERE
	}
	
	/**
	 Checks the board for internal consistency -- used
	 for debugging.
	 
	*/
	public void sanityCheck()throws RuntimeException{
		if (DEBUG) {
			int[] widths1=new int[height];
			int[] heights1=new int[width];
			int maxHeight1;
		   for(int i=0;i<width;i++)
		   {
			   for(int j=0;j<height-1;j++)
			   {
				   if(grid[i][j]==true)
				   {
					   widths1[j]++;
					   heights1[i]=j+1;
				   }
			   }
		   }
		   
		   maxHeight1=heights1[0];
		   for(int i=0;i<width;i++)
		   {
		      if(heights1[i]>maxHeight1)
			    maxHeight1=heights1[i];
		   }
		   for(int i=0;i<width;i++)
		   {
			   if(heights[i]!=heights1[i])throw new RuntimeException("heights error");
		   }
		   
		   
		   for(int i=0;i<height;i++)
		   {
			   if(widths[i]!=widths1[i])throw new RuntimeException("widths error");
		   }
		   if(maxHeight!=maxHeight1)throw new RuntimeException("maxHeight error");
		}
	}
	
	/**
	 Given a piece and an x, returns the y
	 value where the piece would come to rest
	 if it were dropped straight down at that x.
	 
	 <p>
	 Implementation: use the skirt and the col heights
	 to compute this fast -- O(skirt length).
	 
	*/
	public int dropHeight(Piece piece, int x) {
	    //sanityCheck();
//	    int[] skirt = piece.getSkirt();
//		int y=0;
//		y=heights[x];
//		for(int i=0;i<piece.getWidth();i++)
//		{
//		    if(skirt[i]==0 && y<heights[x+i])
//		    	y=heights[x+i];
//		    if()
//		    	 
//		}
		
		int maxY = 0;
		
		for (int i = 0; i < piece.getWidth(); ++i) {
			int skirt = piece.getSkirt()[i];
			
			if (maxY < heights[x + i] - skirt)
				maxY=heights[x+i]-skirt;
		}
		
		return maxY; // YOUR CODE HERE
	}
	
	
	/**
	 Returns the height of the given column --
	 i.e. the y value of the highest block + 1.
	 The height is 0 if the column contains no blocks.
	 
	*/
	public int getColumnHeight(int x) {
		int i;
		for(i=height-1;i>=0;i--)
		{
		     if(grid[x][i]==true) break;
			    //return i+1;
		}
		
		return i+1; // YOUR CODE HERE
	}
	
	
	/**
	 Returns the number of filled blocks in
	 the given row.
	 
	*/
	public int getRowWidth(int y) {
	     int blocks=0;
		 for(int i=0;i<width;i++)
		 {
		      if(grid[i][y]==true)
			        blocks+=1;
		 }
		 return blocks; // YOUR CODE HERE
	}
	
	
	/**
	 Returns true if the given block is filled in the board.
	 Blocks outside of the valid width/height area
	 always return true.
	 
	*/
	public boolean getGrid(int x, int y) {
	    if(x>=0 && x<width)
		{
		     if(y>=0 && y<height)
			 {     
				   return grid[x][y];
			 }
		}
		return true; // YOUR CODE HERE
	}
	
	
	public static final int PLACE_OK = 0;
	public static final int PLACE_ROW_FILLED = 1;
	public static final int PLACE_OUT_BOUNDS = 2;
	public static final int PLACE_BAD = 3;
	
	/**
	 Attempts to add the body of a piece to the board.
	 Copies the piece blocks into the board grid.
	 Returns PLACE_OK for a regular placement, or PLACE_ROW_FILLED
	 for a regular placement that causes at least one row to be filled.
	 
	 <p>Error cases:
	 A placement may fail in two ways. First, if part of the piece may falls out
	 of bounds of the board, PLACE_OUT_BOUNDS is returned.
	 Or the placement may collide with existing blocks in the grid
	 in which case PLACE_BAD is returned.
	 In both error cases, the board may be left in an invalid
	 state. The client can use undo(), to recover the valid, pre-place state.
	 
	*/
	public int place(Piece piece, int x, int y) {
		if (!committed) throw new RuntimeException("place commit problem");
			
		//copy grid[][]
	    for(int i=0;i<width;i++)
	    {
	    	for(int j=0;j<height;j++)
	    	{
	    		
	    		gridB[i][j]=grid[i][j];
	    		//heightsB[i]=heights[i];
	    		
	    	}
	    }
		
		int result = PLACE_OK;
		
		if(x<0||x>getWidth()||y<0||y>getHeight()||(x+piece.getWidth())>getWidth()||(y+piece.getHeight())>getHeight())
			return PLACE_OUT_BOUNDS;
	    //x,y nam trong bang
		TPoint[] body=piece.getBody();
		int[] skirt=piece.getSkirt();
	
		for(int i=0;i<body.length;i++)
		{
			if(grid[x+body[i].x][y+body[i].y]==true)
				return PLACE_BAD;
		}
		
		for(int i=0;i<body.length;i++)
		{
			grid[x+body[i].x][y+body[i].y]=true;
			widths[y+body[i].y]++;
			//heights[x+body[i].x]++;
			if(heights[x+body[i].x]<=y+body[i].y)
				heights[x+body[i].x]=y+body[i].y+1;
		}
		
		int maxH=getMaxHeight();
		maxHeight=maxH;
		
		committed=false;
		
		//sanityCheck();
		for(int i=0;i<height;i++)
		{
			if(widths[i]==width)
				return PLACE_ROW_FILLED;
			else
				return PLACE_OK;
				
		}
		sanityCheck();
		// YOUR CODE HERE
		
		
		return result;
	}
	
	
	/**
	 Deletes rows that are filled all the way across, moving
	 things above down. Returns the number of rows cleared.
	
	*/
	public int clearRows() {
		int rowsCleared = 0;
		for(int i=0;i<getMaxHeight();i++)
		{
			if(getRowWidth(i)==width)
			{   
				rowsCleared++;
				//xoa dong
				for(int j=0;j<width;j++)
				{
					grid[j][i]=false;
					
				}
				
				//chep tung hang den vi tri moi
				for(int k=i;k<getMaxHeight();k++)
				{
					for(int v=0;v<getWidth();v++)
					{
						if(grid[v][k+1]==true)
						{
							grid[v][k]=grid[v][k+1];
							grid[v][k+1]=false;
						}
					}
				}
			 i--;   
			}
		}
		//cap nhat lai mang widths
		int[] widths1=new int[height];
		for(int i=0;i<getMaxHeight();i++)
		{
			for(int j=0;j<getWidth();j++)
			{
				if(grid[j][i]==true)
					widths1[i]++;
					
			}
		}
		widths=widths1;
		
		//cap nhat lai mang heights 
		for(int i=0;i<getWidth();i++)
		{
			heights[i]=heights[i]-rowsCleared;
		}
		
		maxHeight = maxHeight - rowsCleared;
		
		
		// YOUR CODE HERE
		//sanityCheck();
		return rowsCleared;
	}



	/**
	 Reverts the board to its state before up to one place
	 and one clearRows();
	 If the conditions for undo() are not met, such as
	 calling undo() twice in a row, then the second undo() does nothing.
	 See the overview docs.
	
	*/
	public void undo() {
		// YOUR CODE HERE
		     int[] widths1=new int[height];
			 int[] heights1=new int[width];
			 int maxHeight1;
			 for(int i=0;i<width;i++)
			 {
				  
			    	for(int j=0;j<height;j++)
			    	{
			    		grid[i][j]=gridB[i][j];
			    		
			    	}
			 }
			 for(int i=0;i<width;i++)
			 {
				 for(int j=0;j<height-1;j++)
				 {
					 if(grid[i][j]==true)
					 {
						 widths1[j]++;
						 heights1[i]=j+1;
					 }
				 }
			 }
			 widths=widths1;
			 heights=heights1;
			 
			 maxHeight1=heights1[0];
			 for(int i=0;i<width;i++)
			 {
				   if(heights1[i]>maxHeight1)
					   maxHeight1=heights1[i];
			 }
			 
			 maxHeight=maxHeight1;
		     commit();
	}
	
	
	/**
	 Puts the board in the committed state.
	*/
	public void commit() {
		committed = true;
	}


	
	/*
	 Renders the board state as a big String, suitable for printing.
	 This is the sort of print-obj-state utility that can help see complex
	 state change over time.
	 (provided debugging utility) 
	 */
	public String toString() {
		StringBuilder buff = new StringBuilder();
		for (int y = height-1; y>=0; y--) {
			buff.append('|');
			for (int x=0; x<width; x++) {
				if (getGrid(x,y)) buff.append('+');
				else buff.append(' ');
			}
			buff.append("|\n");
		}
		for (int x=0; x<width+2; x++) buff.append('-');
		return(buff.toString());
	}

}
